# Wiki Link Finder

Takes as input a file containing the source code of a Wikipedia page.
Returns a list of the pages for which there is a link.# Sierpinski Diamond

![screenshot](https://gitlab.com/fbarn/wiki-link-finder/raw/master/example.png)
